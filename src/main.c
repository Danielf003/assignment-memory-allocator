#include <stdio.h>
#include <unistd.h>

#include "mem.h"

int getpagesize(void);

void test1(uint8_t* b1){
    fprintf(stdout, "\n\t\tUsual allocation\n");
    b1  = _malloc(sizeof(*b1));
    *b1 = 0x11;
}

void test2(uint8_t* b1, uint8_t* b2, uint8_t* b3, uint8_t* b4){
    fprintf(stdout, "\n\t\tFree 1 from few\n");
    b1  = _malloc(sizeof(*b1));
    b2  = _malloc(sizeof(*b2));
    b3  = _malloc(1024);
    b4  = _malloc(sizeof(*b4));
    *b1 = 0x11;
    *b2 = 0x22;
    *b3 = 0x33;
    *b4 = 0xFF;
    _free(b2);
}

void test3(uint8_t* b1, uint8_t* b2, uint8_t* b3, uint8_t* b4){
    fprintf(stdout, "\n\t\tFree 2 from few\n");
    b1  = _malloc(sizeof(*b1));
    b2  = _malloc(sizeof(*b2));
    b3  = _malloc(1024);
    b4  = _malloc(sizeof(*b4));
    *b1 = 0x11;
    *b2 = 0x22;
    *b3 = 0x33;
    *b4 = 0xFF;
    _free(b3);
    _free(b2);
}

void test4(uint8_t* b1, uint8_t* b2){
    fprintf(stdout, "\n\t\tOutOfMem, continious grow\n");
    b1 = _malloc(2*getpagesize()-20);
    b2 = _malloc(101);
    *b1 = 0x11;
    *b2 = 0x22;
}

void test5(uint8_t* b1, uint8_t* b2){
fprintf(stdout, "\n\t\tOutOfMem, discrete grow\n");
    b1 = _malloc(3*getpagesize());
    b2 = _malloc(6*getpagesize());
    *b1 = 0x11;
    *b2 = 0x22;
}

int main() {
    void* heap = heap_init(1024);
    uint8_t* b1 = NULL;
    uint8_t* b2 = NULL;
    uint8_t* b3 = NULL;
    uint8_t* b4 = NULL;
    
    test1(b1);
    debug_heap(stdout, heap);
    _free_all(heap);

    test2(b1, b2, b3, b4);
    debug_heap(stdout, heap);
    _free_all(heap);
    
    test3(b1, b2, b3, b4);
    debug_heap(stdout, heap);
    _free_all(heap);
    
    test4(b1, b2);
    debug_heap(stdout, heap);
    _free_all(heap);
    debug_heap(stdout, heap);
    
    skip_pages_and_map(heap, 5, 0, getpagesize());
    test5(b1, b2);
    debug_heap(stdout, heap);
    _free_all(heap);
    debug_heap(stdout, heap);
    
    return 0;
}

