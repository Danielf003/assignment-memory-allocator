#ifndef _MEM_H_
#define _MEM_H_

#include <stdint.h>

#define HEAP_START ((void*)0x04040000)

void* _malloc( size_t query );
void  _free( void* mem );
void _free_all(void *mem);
void* heap_init( size_t initial_size );
void* skip_pages_and_map(const void* skip_from, size_t page_count, size_t extra_offset, size_t length);

#define DEBUG_FIRST_BYTES 4

void debug_struct_info( FILE* f, void const* address );
void debug_heap( FILE* f,  void const* ptr );

#endif
